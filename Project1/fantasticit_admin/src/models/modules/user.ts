import { login } from "@/service";
import { ILoginForm } from "@/types";
import { removeToken, setToken } from "@/utils";
import { makeAutoObservable, runInAction } from "mobx"

class User{
    isLogin = false;
    userInfo = {};
    constructor(){
        makeAutoObservable(this);
    }

    async login(data: ILoginForm){
        let result = await login(data);
        console.log('result...', result);
        if (result.data){
            runInAction(()=>{
                this.isLogin = true;
                this.userInfo = result.data;
            })
            // 存储登陆态
            setToken(result.data.token);
        }
        return result.data;
    }

    logout(){
        this.isLogin = false;
        this.userInfo = {};
        removeToken();
    }
}

export default User;