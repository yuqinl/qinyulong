import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
  // 引入antd
  antd: {

  },
  // 引入dva
  dva: {
    immer: true,
    hmr: true,
  }
});
