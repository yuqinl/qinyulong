import React, { useEffect } from 'react';
import { Spin, Select } from 'antd'
import { IRootState } from '@/types';
import { NavLink, useIntl, useDispatch, useSelector, setLocale } from 'umi';
import styles from './index.less';
import MyHeader from '@/components/MyHeader';
import Loading from '@/components/Loading';

const Layouts: React.FC = (props) => {
    const intl = useIntl()
    const dispatch = useDispatch();
    const { global, language } = useSelector((state: IRootState) => {
        return {
            global: state.loading.global,
            language: state.language
        }
    })
    useEffect(()=>{
        setLocale(language.locale, false);
    }, [language.locale]);

    function changeLocale(locale: string){
        dispatch({ 
            type: 'language/save',
            payload: {locale}
        });
    }
    const menus = [{
        title: 'menu.artilce',
        link: '/'
    }, { 
        title: 'menu.archives',
        link: '/archives'
    }, { 
        title: 'menu.knowledge',
        link: '/knowledge'
    }, { 
        title: 'menu.message',
        link: '/messsage'
    }, { 
        title: 'menu.about',
        link: '/about'
    }]
    
    return <div>
        {global && <Loading />}
        {/* 公共头部 */}
        <header>
            <ul>{
                menus.map(item=>{
                    return <NavLink key={item.link} to={item.link}>{intl.formatMessage({
                        id: item.title
                    })}</NavLink>
                })
            }</ul>
            <Select value={language.locale} onChange={e=>changeLocale(e)}>{
                language.locales.map(item=>{
                    return <Select.Option key={item.locale} value={item.locale}>{item.label}</Select.Option>
                })    
            }</Select>
        </header>
        <MyHeader></MyHeader>

        {/* 路由区域 */}
        <main className={styles.main}>
            <div className="container">
                {props.children}
            </div>
        </main>
       
        {/* 公共底部 */}
        <footer>
            底部
        </footer>
    </div>
}

export default Layouts;