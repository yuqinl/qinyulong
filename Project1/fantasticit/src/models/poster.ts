import { genePoster } from '@/services/modules/poster';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface PosterModelState {
  url: string;
}

export interface PosterModelType {
  namespace: 'poster';
  state: PosterModelState;
  effects: {
      genePoster: Effect;
  }
  reducers: {
    save: Reducer<PosterModelState>;
  };
}

const PosterModel: PosterModelType = {
  namespace: 'poster',
  state: {
    url: ''
  },
  effects: {
      *genePoster({payload}, { call, put}){
        let result = yield genePoster(payload);
        if (result.success === true) {
            yield put({
              type: 'save',
              payload: {
                url: result.data.url
              }
            })
          }
      }
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default PosterModel;