import { IArticleItem, IArticleComment, IArticleDetail, IRootState } from '@/types';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getArticleComment, getArticleDetail, getArticleList, getRecommend } from '@/services';

export interface ArticleModelState {
  recommend: IArticleItem [];
  articleList: IArticleItem [];
  articleCount: number;
  articleComment: IArticleComment [];
  articleCommentCount: number;
  articleDetail: Partial<IArticleDetail>;
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  // 异步action，相当于vuex中的action
  effects: {
    getRecommend: Effect;
    getArticleList: Effect;
    getArticleDetail: Effect;
    getArticleComment: Effect;
  };
  // 同步action，相当于vuex中的mutation
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}

const ArticleModel: ArticleModelType = {
  namespace: 'article',
  state: {
    recommend: [],
    articleList: [],
    articleCount: 0,
    articleComment: [],
    articleCommentCount: 0,
    articleDetail: {}
  },

  effects: {
    *getRecommend({payload}, { call, put }) {
      let result = yield call(getRecommend);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {recommend: result.data}
        })
      }
    },
    *getArticleList({payload}, { call, put }) {
      let result = yield call(getArticleList, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            articleList: result.data[0],
            articleCount: result.data[1]
          }
        })
      }
    },
    *getArticleDetail({payload}, { call, put }) {
      let result = yield call(getArticleDetail, payload);
      console.log('result...', result);
      if (result.success === true) {
        yield put({
          type: 'save',
          payload: {
            articleDetail: result.data
          }
        })
      }
    },
    *getArticleComment({payload}, { call, put, select }) {
      let preArticleComment = yield select((state:IRootState)=>state.article.articleComment);
      let result = yield call(getArticleComment, payload.id, payload.page);
      console.log('result...', result);
      if (result.success === true) {
        let articleComment = result.data[0];
        if (payload.page !== 1){
          articleComment = [...preArticleComment, ...articleComment];
        }
        yield put({
          type: 'save',
          payload: {
            articleComment,
            articleCommentCount: result.data[1]
          }
        })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  }
};

export default ArticleModel;