import styles from './index.less';
import { Spin } from 'antd'

const Loading = ()=>{
    return  <div className={styles.loading}><Spin size="large"/></div>
}

export default Loading;