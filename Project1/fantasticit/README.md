# umi project

## Getting Started

Install dependencies,

```bash
$ yarn
```

Start the dev server,

```bash
$ yarn start
```

常用hooks
```js
自带的勾子：useState, useEffect
路由的勾子：useHistory, useMatch
redux的勾子：useDispatch, useSelector
antd的form：useForm
国际化的勾子：useIntl
```

