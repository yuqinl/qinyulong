class Dep{
    constructor(){
        this.subs = [];
    }
    addSub(sub){
        let index = this.subs.indexOf(sub);
        if (index === -1){
            this.subs.push(sub);
        }
    }
    notify(){
        this.subs.forEach(cb=>cb.update());
    }
}

export default Dep;